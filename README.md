# Cut Guide

## One-Sentence-Pitch

Helps keep your fingers intact. 

## How does it work?

To get our samples out of the Beem capsules (size 00 from Science Services #E70020-B) we cut these capsules open with a razor blade and to avoid cutting our fingers we use this cut guide.

<img src="CutGuideRendering.jpg" width=60% />

We embed the samples in Durcupan, an epoxy resin. The liquid resin penetrates the sample, fills the cavities, and is then polymerized at 60°C. This stabilizes the structure and makes the sample cutable (in the nanometer range, between 40 and 100 nm). This is more or less a standard workflow. See https://mmegias.webs.uvigo.es/02-english/6-tecnicas/3-resina.php (for illustration only as they use a different form of embedding). 


<img src="CutGuide01.jpg" width=100% />

How to use it:

Insert capsule from the top and push it in firmly. (The cutting aid is symmetrical and works for both right-handed and left-handed people.)
<img src="CutGuide02.jpg" width=100% />
Press down and cut.
<img src="CutGuide03.jpg" width=100% />
Eject the capsule by inserting a pencil from the other end and pushing it out.  
<img src="CutGuide04.jpg" width=100% />

## Status

In use. 

## Acknowledgment

Vanessa Zheden for testing and valuable feedback.

This project has been made possible in part by grant number 2020-225401 from the Chan Zuckerberg Initiative DAF, an advised fund of Silicon Valley Community Foundation.